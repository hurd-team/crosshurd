#!/bin/bash

if test "$#" != "4" ; then
  echo Usage: `basename $0` TARGET DEB_TARGET_GNU_CPU DEB_TARGET_GNU_SYSTEM RELEASE
  exit 1
fi

STARTDIR=`pwd`
TARGET=`readlink -f $1`
DEB_TARGET_GNU_CPU=$2
DEB_TARGET_GNU_SYSTEM=$3
RELEASE=$4

if [ x"$TARGET" = x ]; then
  echo "Refused to overwrite inexistent/empty TARGET."
  exit 1
fi

. /usr/share/crosshurd/functions

debs=`cat /etc/crosshurd/packages/{common,$DEB_TARGET_GNU_SYSTEM,$DEB_TARGET_GNU_SYSTEM-$DEB_TARGET_GNU_CPU} 2> /dev/null | grep -v '^#'`

target_setup 

setup_etc 

apt_setup

if [ $retval != 0 ]; then
  echo "Failed to download the Packages files."
  exit 1
fi

apt_fetch_deb $debs

if [ $retval != 0 ]; then
  echo "Failed to download the .deb's."
  exit 1
fi

extract `(cd $TARGET/var/cache/apt/archives && \
	for i in *.deb ; do \
		if [ -z "${i##libc0.3_*}" ] || \
		   [ -z "${i##libcrypt*}" ] || \
		   [ -z "${i##libtinfo*}" ] || \
		   [ -z "${i##debianutils_*}" ] || \
		   [ -z "${i##gnumach-image-*}" ] || \
		   dpkg-deb --field $i Priority | grep ^required ; then \
			echo $i ; \
		fi ; \
	done) | cut -d _ -f 1`

# place symlink for compatibility with update-grub (/etc/grub.d/30_os-prober)
( cd $TARGET/boot ; ln -s $( ls gnumach-*.gz | tail -n 1 ) gnumach.gz )

if [ -e $TARGET/var/cache/apt/archives/libbz2-1.0_* ] ; then
        extract libbz2-1.0
fi

if [ -e $TARGET/var/cache/apt/archives/libparted2_* ] ; then
        extract libparted2
fi

mv $TARGET/lib/* $TARGET/usr/lib/
for i in $TARGET/lib/* ; do mv $i/* ${i/\/lib\//\/usr\/lib\/}/ ; done
rmdir $TARGET/lib/*
rmdir $TARGET/lib
ln -s usr/lib $TARGET/lib
mv $TARGET/bin/* $TARGET/usr/bin/
rmdir $TARGET/bin
ln -s usr/bin $TARGET/bin
mv $TARGET/sbin/* $TARGET/usr/sbin/
rmdir $TARGET/sbin
ln -s usr/sbin $TARGET/sbin

ln -s which.debianutils $TARGET/usr/bin/which
ln -s bash $TARGET/bin/sh

x_feign_install dpkg
if [ "$DEB_TARGET_GNU_SYSTEM" = "gnu" ] ; then
	x_feign_install hurd

	##############################
	# Prepare initial translator.
	##############################
	# FIXME: Should be done in the Hurd preinst.
	install -d -m 755 -o root -g root $TARGET/servers/socket
	touch $TARGET/servers/exec
	touch $TARGET/servers/startup
	
	#####################################
	# Make sure libexec/runsystem exists
	#####################################
	# Explanation: It is managed by update-alternatives.
	#
	if [ ! -e $TARGET/libexec/runsystem -a -e $TARGET/libexec/runsystem.gnu ] ; then
		cp $TARGET/libexec/runsystem.gnu $TARGET/libexec/runsystem
	fi
	#####################################
	# create passive translators if the 
	# host system is able to
	#####################################
	gnu_setup_passive_translators
fi

cp /usr/share/crosshurd/native-install $TARGET

##################
# Setup intial /etc/passwd
##################
echo "root:x:0:0:root:/root:/bin/bash" > $TARGET/etc/passwd

##################
# Setup hostname
##################
echo "I: Preparing /etc/hostname using `hostname`..."
echo `hostname` > $TARGET/etc/hostname

###################
# Setup /etc/hosts
###################
echo "I: Preparing /etc/hosts..."

echo "127.0.0.1		localhost `hostname`" > $TARGET/etc/hosts

#########################
# Setup /etc/resolv.conf
#########################
echo "I: Preparing /etc/resolv.conf..."

if [ -e /etc/resolv.conf ] ; then
	echo Copying this machine\'s resolv.conf to the new partition.
	echo If this is not correct, please edit.
	cp /etc/resolv.conf $TARGET/etc/resolv.conf
else
	echo Setting up bogus file, PLEASE EDIT.
	echo "domain ${domainname}" > $TARGET/etc/resolv.conf
	echo "search localnet" >> $TARGET/etc/resolv.conf
	echo "nameserver WWW.XXX.YYY.ZZZ" >> $TARGET/etc/resolv.conf
fi

# more possibly interesting hooks from Marcus' cross-hurd follow
# we don't use them for now.

exit 0

#####################################
# Make sure boot/servers.boot exists
#####################################
# Explanation: It is a conf file in some versions of Hurd.

echo
echo "Latest Hurd packages declare boot/servers.boot as a conffile, let me check."

if [ -e ${dest}/boot/servers.boot.dpkg-new ] ; then
	echo "Yes, found it. I will copy boot/servers.boot.dpkg-new to boot/servers.boot"
	cp ${dest}/boot/servers.boot.dpkg-new ${dest}/boot/servers.boot
else
	echo "Nope, not your version. Please make sure that you check boot/servers.boot after"
        echo "your next update of the hurd package, because your changes will be overriden."
fi

#########################
# Make sure sh exists
#########################
# Explanation: Thorsten did a strange bash NMU.
if [ ! -e ${dest}/bin/sh ] ; then
	echo "Creating symlink /bin/sh."
	ln -s bash ${dest}/bin/sh
fi

############################
# Move perl in proper place
############################
# Explanation: Latest glibc package needs perl (and awk) to configure!

if [ ! -e ${dest}/usr/bin/perl ] ; then
	if [ -e ${dest}/usr/bin/perl.dist ] ; then
		echo
		echo "Moving perl into place."
		mv ${dest}/usr/bin/perl.dist ${dest}/usr/bin/perl
	else
		if [ -e ${dest}/usr/bin/perl-5.005.dist ] ; then
		echo
		echo "Found versioned perl."
		# We only copy it here so the postinst does the right thing.
		# A mv or ln won't work!
		cp ${dest}/usr/bin/perl-5.005.dist ${dest}/usr/bin/perl
		else
			echo
			echo "Warning, I could not find perl!"
		fi
	fi
fi

########################################################
# make some essential directories for configure of dpkg
########################################################
# FIX: In sysvutils, together with split-init
if [ ! -e ${dest}/etc/rc0.d ] ; then
        echo
        echo "Making etc/rc?.d directories for dpkg."
        for nr in 0 1 2 3 4 5 6 S
        do
                install -d -m 755 -o root -g root ${dest}/etc/rc${nr}.d
        done
        install -d -m 755 -o root -g root ${dest}/etc/rc.boot
fi
