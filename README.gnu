Welcome to crosshurd!
=====================

crosshurd can now be used to install not only GNU, but also GNU/Linux and
GNU/kFreeBSD.

The instructions below apply only to Hurd-based GNU system.

1) Create a partition, and put a file system on it. If you are planning
on using ext2, the command:

	mke2fs -o hurd /dev/hda12

(substituting the right partition) is usually good. The -o hurd is
necessary to set appropriate inode and block size and so that the system know
that it's safe to setup translators.

2) Mount it somewhere.

3) Review /etc/crosshurd/sources.list/gnu. The Hurd often requires a few
secondary archives to be added to get all the needed packages. I've
tried to include sensible defaults.

4) Run /usr/sbin/crosshurd

This will ask where the partition is mounted.

Not all packages downloaded by apt will be installed, only Priority: required
ones will be.

5) Setup grub to boot from the new partition. If you don't know how
to do this, go out and grab some of Philip's CDs and start with them.
Explaining grub is out of scope of this README. Make sure not to pass
--readonly to ext2fs.static.

6) On the first boot, run :

	./native-install

And when it's done:

	reboot

You may see error messages about not being able to chown to root.  These
are being worked on and are safe to ignore.

7) Reboot, and enjoy.


